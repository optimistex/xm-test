import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ToastModule} from 'ng2-toastr';

import {ChartModule} from 'angular2-highcharts';
import {HighchartsStatic} from 'angular2-highcharts/dist/HighchartsService';

import {MyDatePickerModule} from 'mydatepicker';
import {ApiService} from "./_service/api.service";
import {AppComponent} from './app.component';

declare var require;

export function highchartsFactory() {
    const hc = require('highcharts/highstock');
    const dd = require('highcharts/modules/exporting');
    dd(hc);
    return hc;
}

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule,
        HttpClientModule,
        FormsModule, ReactiveFormsModule,
        MyDatePickerModule,
        ToastModule.forRoot(),
        ChartModule
    ],
    providers: [
        {provide: HighchartsStatic, useFactory: highchartsFactory},
        ApiService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
