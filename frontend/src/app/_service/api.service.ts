import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {ToastsManager} from "ng2-toastr";

interface IMessage {
    code: number;
    text: string;
}

export interface IRestApiResponse {
    [key: string]: any;

    success?: boolean;
    messages?: IMessage[];
}

@Injectable()
export class ApiService {

    // readonly baseUrl = 'http://api.xm-test.local/index.php?r=';
    readonly baseUrl = 'http://localhost:8080/index.php?r=';
    private toastOptions = {showCloseButton: true, toastLife: 5000};

    constructor(private http: HttpClient, private toast: ToastsManager) {
    }

    public post<T>(url: string, data: any): Observable<T> {
        return this.http
            .post<T>(this.baseUrl + url, data, {withCredentials: true})
            .do((data: IRestApiResponse) => {
                if (typeof data === 'object' && data.messages) {
                    for (const i in data.messages) {
                        if (data.messages.hasOwnProperty(i)) {
                            this.showMessage(data.messages[i]);
                        }
                    }
                }
            })
            .catch((error: HttpErrorResponse) => {
                this.toast
                    .error(
                        `<br><b>${this.baseUrl}${url}</b><br><br>` +
                        error.message,
                        error.status + ' ' + error.name,
                        Object.assign(this.toastOptions, {enableHTML: true})
                    )
                    .catch(r => console.log(r));
                return Observable.empty<any>();
            });
    }

    private showMessage(message: IMessage) {
        const code = message.code, text = message.text;

        if (code >= 4000 || (code >= 400 && code < 600)) {
            this.toast.error(text, code.toString(), this.toastOptions).catch(r => console.log(r));
            console.log('error', code, text);
        } else if (code >= 3000) {
            this.toast.warning(text, code.toString(), this.toastOptions).catch(r => console.log(r));
            console.log('warning', code, text);
        } else if (code >= 2000) {
            this.toast.success(text, code.toString(), this.toastOptions).catch(r => console.log(r));
            console.log('success', code, text);
        } else {
            this.toast.info(text, code.toString(), this.toastOptions).catch(r => console.log(r));
            console.log('info', code, text);
        }
    }

}
