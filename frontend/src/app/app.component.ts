import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {ToastsManager} from "ng2-toastr";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import {ApiService, IRestApiResponse} from "./_service/api.service";

interface IOHLC {
    date: number;
    open: number;
    high: number;
    low: number;
    close: number;
    volume: number;
}

interface IHistoricalQuotes extends IRestApiResponse {
    quotes: IOHLC[];
}

interface IOptionsRequest {
    companySymbol: string,
    dateStart: number,
    dateEnd: number,
    email: string
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    public form: FormGroup;
    public options: any;
    public hQuotes: IOHLC[];

    constructor(private toasts: ToastsManager, containerRef: ViewContainerRef, private api: ApiService) {
        this.toasts.setRootViewContainerRef(containerRef);
    }

    ngOnInit(): void {
        this.form = new FormGroup({
            companySymbol: new FormControl(null, Validators.required),
            dateStart: new FormControl(null, Validators.required),
            dateEnd: new FormControl(null, Validators.required),
            email: new FormControl(null, [
                Validators.required, AppComponent.validatorEmail()
            ]),
        });
    }

    public onSubmit() {
        const formValue = this.form.value;
        this.getOptions({
            companySymbol: formValue['companySymbol'],
            dateStart: formValue['dateStart']['formatted'],
            dateEnd: formValue['dateEnd']['formatted'],
            email: formValue['email']
        });
    }

    public controlMessage = (control: FormControl): string[] => {
        const validateMessages = {
                required: 'Required to fill',
                email: 'Invalid e-mail'
            },
            errors = [];
        for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
                errors.push(validateMessages[key] || 'Unknown err: ' + key);
            }
        }
        return errors;
    };

    private static validatorEmail(): ValidatorFn {
        const regex = /(.+)@(.+){2,}\.(.+){2,}/;
        return (control: AbstractControl) => {
            return control.value === null || regex.test(control.value) ? null : {'email': true};
        };
    };

    private getOptions(request: IOptionsRequest) {
        this.api
            .post<IHistoricalQuotes>('api/historical-quotes', request)
            .filter((data: IHistoricalQuotes) => Array.isArray(data.quotes))
            .map((data: IHistoricalQuotes) => {
                this.hQuotes = data.quotes;

                const res = {ohlc: [], volume: []};
                data.quotes.forEach((quote: IOHLC) => {
                    res.ohlc.push([quote.date * 1000, quote.open, quote.high, quote.low, quote.close]);
                    res.volume.push([quote.date * 1000, quote.volume]);
                });
                return res;
            })
            .subscribe((data: { ohlc: any[]; volume: any[] }) => this.options = {
                rangeSelector: {selected: 1},
                title: {text: this.form.get('companySymbol').value + ' Stock Price'},
                yAxis: [
                    {
                        labels: {align: 'right', x: -3},
                        title: {text: 'OHLC'},
                        height: '60%',
                        lineWidth: 2,
                        resize: {enabled: true}
                    },
                    {
                        labels: {align: 'right', x: -3},
                        title: {text: 'Volume'},
                        top: '65%',
                        height: '35%',
                        offset: 0,
                        lineWidth: 2
                    }
                ],
                tooltip: {split: true},
                series: [
                    {type: 'candlestick', name: 'AAPL', data: data.ohlc},
                    {type: 'column', name: 'Volume', data: data.volume, yAxis: 1}
                ]
            });
    }

}
