<?php
/**
 * Created by PhpStorm.
 * User: optimistex
 * Date: 24.03.18
 * Time: 20:42
 */

namespace app\models;

use Yii;
use app\helpers\MessageHelper;

/**
 * Class HistoricalQuotes
 *
 * @property integer $dateStartInt
 * @property integer $dateEndInt
 *
 * @package app\models
 */
class HistoricalQuotes extends RestModel
{
    /**
     * @var string
     */
    public $companySymbol;

    /**
     * @var string
     */
    public $dateStart;

    /**
     * @var string
     */
    public $dateEnd;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string[]
     */
    private $srcQuotesClasses = [HQuotesQuandlSource::class, HQuotesWsjSource::class];

    /** @inheritdoc */
    public function rules()
    {
        return [
            [['companySymbol', 'dateStart', 'dateEnd', 'email'], 'required'],
            [['companySymbol', 'dateStart', 'dateEnd'], 'string'],
            ['email', 'email'],
            [['dateStart', 'dateEnd'], 'date', 'format' => 'php:Y-m-d', 'skipOnEmpty' => false],
            ['companySymbol', function ($attribute) {
                $symbolList = include __DIR__ . '/data/companySymbols.php';
                if (!in_array($this->companySymbol, $symbolList, true)) {
                    $this->addError($attribute, 'Unknown company symbol');
                }
            }]
        ];
    }

    public function afterValidate()
    {
        if (!$this->hasErrors() && $this->dateStartInt > $this->dateEndInt) {
            $this->addError('dates', 'Date start must be less than the date end');
        }

        parent::afterValidate();
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        $cacheKey = [$this->companySymbol, $this->dateStart, $this->dateEnd];
        $data = Yii::$app->cache->get($cacheKey);
        if ($data === false) {
            foreach ($this->srcQuotesClasses as $srcQuotesClass) {
                /** @var HQuotesSourceBase $srcQuotes */
                $srcQuotes = new $srcQuotesClass($this->companySymbol, $this->dateStartInt, $this->dateEndInt);
                if (($data = $srcQuotes->getData()) !== false) {
                    break;
                }
            }
            Yii::$app->cache->set($cacheKey, $data);
        } else {
            MessageHelper::addCustomInfo('The data got from a cache');
        }
        return $data;
    }

    /**
     * @return int
     */
    public function getDateStartInt(): int
    {
        $date = \DateTime::createFromFormat('Y-m-d', $this->dateStart);
        return $date ? $date->getTimestamp() : 0;
    }

    /**
     * @return int
     */
    public function getDateEndInt(): int
    {
        $date = \DateTime::createFromFormat('Y-m-d', $this->dateEnd);
        return $date ? $date->getTimestamp() : 0;
    }
}