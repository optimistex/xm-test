<?php
/**
 * Created by PhpStorm.
 * User: optimistex
 * Date: 25.03.18
 * Time: 6:00
 */

namespace app\models;

/**
 * Get data from Wall Street Journal
 * Class HQuotesWsjSource
 * @package app\models
 */
class HQuotesWsjSource extends HQuotesSourceBase
{
    protected function buildUrl(): string
    {
        // http://quotes.wsj.com/AAPL/historical-prices/download?MOD_VIEW=page&num_rows=5000&startDate=09/06/2000&endDate=12/05/2017
        $dateStart = date('m/d/Y', $this->dateStart);
        $endDate = date('m/d/Y', $this->dateEnd);
        $numRows = ($this->dateEnd - $this->dateStart) / 86400;
        return 'http://quotes.wsj.com/' . $this->companySymbol . '/historical-prices/download?MOD_VIEW=page&'
            . "num_rows={$numRows}&startDate={$dateStart}&endDate={$endDate}";
    }

    protected function decodeDateValue(string $value): int
    {
        return \DateTime::createFromFormat('m/d/y', $value)->getTimestamp();
    }

    protected function sortData(&$data): void
    {
        uasort($data, function ($d1, $d2) {
            return $d1['date'] - $d2['date'];
        });
    }
}