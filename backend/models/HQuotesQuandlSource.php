<?php
/**
 * Created by PhpStorm.
 * User: optimistex
 * Date: 25.03.18
 * Time: 5:59
 */

namespace app\models;

/**
 * Get data from Quandl
 * Class HQuotesQuandlSource
 * @package app\models
 */
class HQuotesQuandlSource extends HQuotesSourceBase
{
    protected function buildUrl(): string
    {
        // https://www.quandl.com/api/v3/datasets/WIKI/AAPL.csv?order=asc&start_date=2003-01-01&end_date=2003-03-06
        $dateStart = date('Y-m-d', $this->dateStart);
        $endDate = date('Y-m-d', $this->dateEnd);
        return 'https://www.quandl.com/api/v3/datasets/WIKI/' . $this->companySymbol . '.csv?order=asc&'
            . "start_date={$dateStart}&end_date={$endDate}";
    }

    protected function decodeDateValue(string $value): int
    {
        return \DateTime::createFromFormat('Y-m-d', $value)->getTimestamp();
    }
}