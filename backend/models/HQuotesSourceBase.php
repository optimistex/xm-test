<?php
/**
 * Created by PhpStorm.
 * User: optimistex
 * Date: 25.03.18
 * Time: 5:58
 */

namespace app\models;

use GuzzleHttp\Client;
use League\Csv\Reader;
use app\helpers\MessageHelper;

abstract class HQuotesSourceBase
{
    protected $companySymbol;
    protected $dateStart;
    protected $dateEnd;

    public function __construct(string $companySymbol, int $startDate, int $endDate)
    {
        $this->companySymbol = $companySymbol;
        $this->dateStart = $startDate;
        $this->dateEnd = $endDate;
    }

    /**
     * @return array|false
     */
    public function getData()
    {
        try {
            // download CSV
            $res = (new Client())->request('GET', $this->buildUrl());
            $csvData = $res->getBody()->getContents();

            // parse CSV
            $csv = Reader::createFromString($csvData);
            $csv->setHeaderOffset(0);
            $csvAsArray = $csv->getRecords(['Date', 'Open', 'High', 'Low', 'Close', 'Volume']);

            // prepare OHLCV data
            $data = [];
            foreach ($csvAsArray as $row) {
                $data[] = [
                    'date' => $this->decodeDateValue($row['Date']),
                    'open' => floatval($row['Open']),
                    'high' => floatval($row['High']),
                    'low' => floatval($row['Low']),
                    'close' => floatval($row['Close']),
                    'volume' => floatval($row['Volume']),
                ];
            }

            $this->sortData($data);

            return ['quotes' => array_values($data)];
        } catch (\Exception $e) {
            MessageHelper::addCustomError($e->getMessage());
            return false;
        }
    }

    abstract protected function buildUrl(): string;

    abstract protected function decodeDateValue(string $value): int;

    protected function sortData(&$data): void
    {
    }
}