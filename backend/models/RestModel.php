<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use app\helpers\MessageHelper;

/**
 * Class RestModel
 * @package lisa10_rest_common\models\base
 */
abstract class RestModel extends Model
{
    /**
     * @param string $attribute атрибут проверка которого вызвала ошибку
     * @param string $error текст ошибки
     */
    public function addError($attribute, $error = '')
    {
        if (is_numeric($error)) {
            $this->addErrorByCode($attribute, $error);
        }

        $error = MessageHelper::addCustomError($error);
        /** @noinspection PhpUndefinedMethodInspection */
        /** @noinspection PhpUndefinedClassInspection */
        parent::addError($attribute, $error);
    }

    /**
     * @param string $attribute атрибут проверка которого вызвала ошибку
     * @param integer $errorCode код ошибки
     * @param array $errorParams массив параметров для формирования текста ошибки в стеке сообщений
     */
    public function addErrorByCode(string $attribute, $errorCode = 4000, array $errorParams = [])
    {
        if (!array_key_exists('param', $errorParams)) {
            $errorParams['param'] = $attribute;
        }
        $error = MessageHelper::add((int)$errorCode, $errorParams);
        /** @noinspection PhpUndefinedMethodInspection */
        /** @noinspection PhpUndefinedClassInspection */
        parent::addError($attribute, $error);
    }

    public function loadGet(bool $validateAfterLoad = true, array $filters = []): bool
    {
        return $this->loadData(Yii::$app->request->get(), $validateAfterLoad, $filters);
    }

    public function loadPost(bool $validateAfterLoad = true, array $filters = []): bool
    {
        return $this->loadData(Yii::$app->request->post(), $validateAfterLoad, $filters);
    }

    private function loadData(array $data, bool $validateAfterLoad, array $filters): bool
    {
        if (!empty($filters)) {
            $data = ArrayHelper::filter($data, $filters);
        }
        return (empty($data) || $this->load($data, ''))
            && (!$validateAfterLoad || $this->validate());
    }
}
