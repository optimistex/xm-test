<?php
/**
 * Created by PhpStorm.
 * User: optimistex
 * Date: 24.03.18
 * Time: 20:49
 */

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\HistoricalQuotes;

class ApiController extends RestController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'rules' => [
                [
                    'actions' => ['historical-quotes'],
                    'allow' => true,
                    'roles' => ['?'],
                ],
            ],
        ];

        return $behaviors;
    }

    public function actionHistoricalQuotes()
    {
        $hq = new HistoricalQuotes();
        if ($hq->loadPost()) {

            Yii::$app->mailer
                ->compose('@app/mail/test-mail.php', ['hq' => $hq])
                ->setFrom('test@xm.com')
                ->setTo($hq->email)
                ->setSubject($hq->companySymbol)
                ->send();

            return $hq->getData();
        }
        return [];
    }
}