<?php

namespace app\controllers;

use Yii;
use yii\filters\Cors;
use yii\filters\VerbFilter;
use yii\filters\RateLimiter;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;
use yii\web\ErrorHandler;
use app\helpers\MessageHelper;

/**
 * 1. It's a base controller implementing RESTfull API paradigm.
 * 2. Fixes issues of processing OPTIONS-request
 */
abstract class RestController extends Controller
{
    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();

        // Disable sessions for REST
        Yii::$app->user->enableSession = false;

        // Setup error handler for response codes of exceptions
        //
        // If we comment out the code
        // then on any exception a browser will get the 500 error
        $handler = Yii::createObject(['class' => ErrorHandler::class]);
        Yii::$app->set('errorHandler', $handler);
        $handler->register();
    }

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    '*' => ['get', 'post', 'options'],
                ],
            ],
            'contentNegotiator' => [
                'class' => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                    'application/xml' => Response::FORMAT_XML,
                ],
            ],
            'rateLimiter' => [
                'class' => RateLimiter::class,
            ],
            'corsFilter' => [
                'class' => Cors::class,
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => ['Content-Disposition'],
                ]
            ],
        ];
    }

    /** @inheritdoc */
    public function afterAction($action, $result)
    {
        $messages = MessageHelper::getMessages();
        if (!empty($messages)) {
            if (is_array($result)) {
                if (array_key_exists('messages', $result) && is_array($result['messages'])) {
                    $result['messages'] = array_merge_recursive($result['messages'], $messages);
                } else {
                    $result['messages'] = $messages;
                }
            } else {
                $result = [
                    'data' => $result,
                    'messages' => $messages
                ];
            }
        }
        return parent::afterAction($action, $result);
    }
}