<?php
/**
 * Created by PhpStorm.
 * User: optimistex
 * Date: 26.03.18
 * Time: 3:02
 */

namespace tests\models;

use app\models\HistoricalQuotes;

class HistoricalQuotesTest extends \Codeception\Test\Unit
{
    public function testValidation()
    {
        $hq = new HistoricalQuotes();
        expect($hq->validate())->false();
        expect($hq->dateStartInt)->equals(0);
        expect($hq->dateEndInt)->equals(0);
        $hq->companySymbol = 'GOOG';
        expect($hq->validate())->false();
        $hq->dateStart = '2017-02-02';
        expect($hq->validate())->false();
        $hq->dateEnd = '2017-02-02';
        expect($hq->validate())->false();
        $hq->email = 'test@test.te';
        expect($hq->validate())->true();

        $hq->companySymbol = 'a';
        expect($hq->validate())->false();
        $hq->companySymbol = 'AAPL';
        expect($hq->validate())->true();

        $hq->dateStart = '2017-04-02';
        expect($hq->validate())->false();
        $hq->dateEnd = '2017-04-02';
        expect($hq->validate())->true();
    }
}