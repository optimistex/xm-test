<?php
/**
 * Created by PhpStorm.
 * User: optimistex
 * Date: 24.05.16
 * Time: 4:29
 */

namespace app\helpers;

use Yii;

class MessageHelper
{
    private static $config;
    private static $messageStack = [];
    private static $messageStackLocker = 0;

    public static function decode($code, array $params = [])
    {
        static::loadConfig();
        if (array_key_exists($code, static::$config)) {
            return Yii::t('message_helper', static::$config[$code], $params);
        }
        return '';
    }

    public static function add($code, array $params = [])
    {
        return static::push($code, null, $params);
    }

    public static function addCustomInfo($message, array $params = [])
    {
        return static::push(1000, $message, $params);
    }

    public static function addCustomSuccess($message, array $params = [])
    {
        return static::push(2000, $message, $params);
    }

    public static function addCustomWarning($message, array $params = [])
    {
        return static::push(3000, $message, $params);
    }

    public static function addCustomError($message, array $params = [])
    {
        return static::push(4000, $message, $params);
    }

    public static function clear()
    {
        static::$messageStack = [];
    }

    /**
     * @return array
     */
    public static function getMessages()
    {
        return static::$messageStack;
    }

    /**
     * @return string
     */
    public static function getMessagesAsString()
    {
        $res = '';
        foreach (static::$messageStack as $message) {
            if (!empty($res)) {
                $res .= "\r\n\r\n";
            }
            $res .= $message['code'] . ': ' . $message['text'];
        }
        return $res;
    }

    /** Lock stack from new error messages */
    public static function lock()
    {
        static::$messageStackLocker++;
    }

    /** Unlock stack for new error messages */
    public static function unlock()
    {
        if (static::$messageStackLocker > 0) {
            static::$messageStackLocker--;
        }
    }

    /**
     * @param integer $code
     * @param string|null $customMessage
     * @param array $params
     * @return string
     */
    private static function push($code, $customMessage, $params)
    {
        static::loadConfig();

        $msgText = '';

        if (array_key_exists($code, static::$config)) {
            if ($customMessage === null) {
                $customMessage = static::$config[$code];
            }

            $msgText = Yii::t('yii', $customMessage, $params);
            if ($code >= 4000) {
                Yii::error($msgText);
            }
            if (static::$messageStackLocker === 0) {
                static::$messageStack[] = [
                    'code' => $code,
                    'text' => $msgText
                ];
            }
            $msgText = $code . ': ' . $msgText;
        }

        return $msgText;
    }

    private static function loadConfig()
    {
        static $needLoad = true;
        if ($needLoad) {
            static::$config = include __DIR__ . '/messages/message_helper.php';
            $needLoad = false;
        }
    }
}