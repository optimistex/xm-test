# The test TASK

This project tested with the next environment:

* OS: [Ubuntu 16.04 LTS](https://www.ubuntu.com/download/desktop)
* PHP 7.1.11 ([used XAMPP](https://www.apachefriends.org/ru/index.html))
* NodeJS 9.6.1: https://nodejs.org
* Composer version 1.5.5 2017-12-01 14:42:57: https://getcomposer.org/

## Installing

1. Install backend:

    ```bash
    cd ./backend
    composer install
    ```

1. Install frontend:

    ```bash
    cd ./frontend
    npm install
    ```
    
## Run servers

1. `serve-backend.sh` - start backend server 
2. `serve-frontend-develop.sh` - start frontend dev-server 
3. `serve-frontend-production.sh` - start production version of the frontend    
4. `test-backend.sh` - run unit tests for the backend    
 
**Warning!** After runnig the backend and the frontend servers use the url `http://localhost:4200` for open the frontend page.

The url `http://localhost:8080` for API requests.

# Description

## Backend

1. Controllers:
    * `\app\controllers\RestController` - basic REST controller. It mast be used for all other RESTfull controllers.
    It resolve CORS. 
    * `\app\controllers\ApiController` - some first controller for implementation some API 
    * `\app\controllers\ApiController::actionHistoricalQuotes` - action to get a data and send the email
         In develop mode from a local machine the email does not sent really, but it works on a real hosting.
         For check that an email is sent look at the `http://localhost:8080/index.php?r=debug`.  

2. Models:
    * `\app\models\HistoricalQuotes` - model for validate and fetch the data
        * `\app\models\HistoricalQuotes::getData` - there is used caching of the data
    * `backend/models/HQuotesSourceBase.php` - the abstract class for downloading a stock data
        * `backend/models/HQuotesQuandlSource.php` - get data from Quandl. 
        Works good but has a limit for 50 maximum connections per one day
        * `backend/models/HQuotesWsjSource.php` - get the same data from Wall Street Journal. 
        Works as alternative source data. It uses if Quandl return error

3. Helpers:
    * `backend/helpers/MessageHelper.php` - helper for collection any massages and attach them to a response
    
4. Tests:
    * `\tests\models\HistoricalQuotesTest` - test validation data
    
## Frontend

1. `frontend/src/app/_service/api.service.ts` - the service for use API requests. 
    * `ApiService.baseUrl` - define the URL prefix for correct requests to the API

2. `frontend/src/app/app.component.ts` - the component implemented frontend of the task

# Examples

Main page:

![Main](img/main.png)
______
Debug of a email sent:

![Main](img/debug-email.png)